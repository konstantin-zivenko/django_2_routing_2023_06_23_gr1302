from django.http import HttpRequest, HttpResponse


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Main route is here.")


def special_case_2023(request: HttpRequest) -> HttpResponse:
    return HttpResponse("it is special case 2023")


def year_archive(request: HttpRequest, year: int) -> HttpResponse:
    return HttpResponse(f"it is arhive for {year}")


def month_archive(request: HttpRequest, year: int, month: int) -> HttpResponse:
    return HttpResponse(f"it is arhive for {year}, month: {month}")


def article_deatail(
    request: HttpRequest, year: int, month: int, slug: str
) -> HttpResponse:
    return HttpResponse(f"arhive for {year}, month: {month}\narticle: {slug}")


def page(request: HttpRequest, num=1) -> HttpResponse:
    return HttpResponse(f"it is my blog, page {num}")
