from django.urls import path, register_converter, re_path
from . import views, converters


register_converter(converters.FourDigitYearConverter, "yyyy")

# urlpatterns = [
#     path("", views.index, name="index"),
#     path("2023/", views.special_case_2023, name="special_case"),
#     path("<yyyy:year>/", views.year_archive , name="year_archive"),
#     path("<yyyy:year>/<int:month>/", views.month_archive , name="month_archive"),
#     path("<yyyy:year>/<int:month>/<slug:slug>/", views.article_deatail, name="article_detail"),
# ]

urlpatterns = [
    path("", views.index, name="index"),
    path("2023/", views.special_case_2023, name="special_case"),
    re_path(r"^(?P<year>[0-9]{4})/$", views.year_archive, name="year_archive"),
    re_path(
        r"^(?P<year>[0-9]{4})/(?P<month>[01]?[0-9]{1})/$",
        views.month_archive,
        name="month_archive",
    ),
    re_path(
        r"^(?P<year>[0-9]{4})/(?P<month>[01]?[0-9]{1})/(?P<slug>[\w-]+)/$",
        views.article_deatail,
        name="article_detail",
    ),
    path("blog/", views.page),
    path("blog/page<int:num>/", views.page),
]

# str - непустий рядок, крім "/"
# int - 0 ....
# slug - a-zA-z0-9-_
# uuid -
# path -
